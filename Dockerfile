FROM python:3.8

WORKDIR /app

RUN apt-get update && apt-get install -y libsndfile-dev

RUN pip install --upgrade pip

RUN python3 -m venv /opt/venv

COPY requirements.txt /app/requirements.txt

COPY . /app

RUN . /opt/venv/bin/activate && pip install -e .

RUN pip install torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
EXPOSE 8000

CMD . /opt/venv/bin/activate && uvicorn main:app --host 0.0.0.0 --port 8000