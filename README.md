# A Vietnamese TTS
================

Tacotron + HiFiGAN vocoder for vietnamese datasets.
## Install
```sh
  pip install -e .
  pip install torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
```
## Run 
```sh
 uvicorn main:app --host 0.0.0.0 --port 8000
 ```

